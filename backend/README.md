```
project-name/
├── cmd/
│   └── app/
│       └── main.go
├── internal/
│   ├── domain/
│   │   ├── entity/
│   │   │   ├── user.go
│   │   └── repository/
│   │       ├── user_repository.go
│   ├── usecase/
│   │   ├── user_usecase.go
│   │   └── interactor/
│   │       ├── user_interactor.go
│   ├── transport/
│   │   ├── handler/
│   │   │   ├── user_handler.go
│   │   │   ├── user_handler_test.go
│   │   │   ├── user_models.go
│   │   └── router.go
│   ├── infrastructure/
│   │   └── repository/
│   │       ├── user_repository_sql.go
│   │       ├── user_repository_s3.go
│   ├── config/
│   │   └── config.go
│   ├── testutils/
│   │   └── setup_router.go
│   └── log/
│       └── logger.go
├── pkg/
│   ├── utils/
│   │   ├── utils.go
│   │   └── xidutil.go
│   ├── constants/
│   │   ├── error_codes.go
│   │   └── messages.go
│   ├── middleware/
│   │   ├── auth.go
│   │   └── logger.go
│   ├── errors/
│   │   └── custom_errors.go
│   └── validation/
│       └── validation.go
├── go.mod
└── go.sum
```

1. cmd: エントリーポイントとなるmain.goファイルを含むディレクトリです。
2. internal: アプリケーションの内部パッケージが含まれるディレクトリです。このディレクトリは、ドメイン、ユースケース、インフラストラクチャに分かれています。
3. domain: ビジネスロジックとデータ構造を定義するディレクトリです。エンティティとリポジトリインターフェースが含まれています。
4. usecase: アプリケーションのユースケースを表現するディレクトリです。ビジネスロジックを実行するためのインターフェースとインタラクターが含まれています。
5. infrastructure: アプリケーションのインフラストラクチャ関連のコードが含まれるディレクトリです。データベース接続、APIハンドラー、設定ファイル、ロガーなどが含まれています。
6. pkg: アプリケーション全体で共通して使用されるユーティリティ関数を含むディレクトリです。
7. constants: 定数を定義するファイルやフォルダ。例えば、エラーコードや共通のメッセージなどを定義する場合に使用します。
8. middleware: 共通のミドルウェアを定義するフォルダ。例えば、認証やロギングなどのミドルウェアを定義する場合に使用します。
9. errors: アプリケーションで共通して使用される独自のエラー型を定義するファイルやフォルダ。カスタムエラー処理を行う場合に使用します。
10. validation: 入力データのバリデーションに関する共通コードを定義するフォルダ。
11. models: アプリケーション全体で共有されるデータモデルや構造体を定義するフォルダ。ただし、ドメインモデルは internal/domain/entities に配置することが推奨されます。
12. test: 各レイヤーの単体テストが含まれるディレクトリです。
13. go.mod と go.sum: Goの依存関係を管理するファイルです。

internal/domain/entity フォルダにエンティティ（UserとProduct）があります。これは、クリーンアーキテクチャの「エンティティ」層に相当します。
internal/usecase フォルダにユースケース（UserUsecaseとProductUsecase）があります。これは、クリーンアーキテクチャの「ユースケース」層に相当します。
internal/transport フォルダには、ハンドラー（UserHandlerとProductHandler）やルーターが含まれており、これはクリーンアーキテクチャの「インタフェースアダプタ」層に相当します。
internal/infrastructure フォルダには、リポジトリの具体的な実装（UserRepositorySQLとProductRepositorySQL）が含まれており、これはクリーンアーキテクチャの「フレームワークとドライバ」層に相当します。
また、pkgフォルダには、共通のユーティリティやミドルウェア、エラーハンドリング、バリデーションなど、アプリケーション全体で使用されるコンポーネントが含まれています。これらは、どのアーキテクチャにも共通する要素です。