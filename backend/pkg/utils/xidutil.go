package utils

import "github.com/rs/xid"

// GenerateID generates a new unique ID as a string.
func GenerateID() string {
	return xid.New().String()
}
