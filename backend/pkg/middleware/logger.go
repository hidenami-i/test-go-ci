package middleware

import (
	"time"

	"github.com/gin-gonic/gin"

	"github.com/template-web-app/internal/log"
)

func Logger() gin.HandlerFunc {
	return func(c *gin.Context) {
		// 開始時刻を記録
		startTime := time.Now()

		// 次のミドルウェアまたはリクエストハンドラーに処理を渡す
		c.Next()

		// 経過時間を計算
		elapsedTime := time.Since(startTime)

		// リクエスト情報をログに出力
		log.Infof("Request processed: method=%s path=%s status=%d elapsed_time=%s",
			c.Request.Method,
			c.Request.URL.Path,
			c.Writer.Status(),
			elapsedTime,
		)
	}
}
