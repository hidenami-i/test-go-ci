-- migrate:up
create table users
(
    id         char(33) primary key,
    first_name varchar(255) not null,
    last_name  varchar(255) not null,
    email      varchar(255) not null,
    password   varchar(255) not null,
    created_at timestamp    not null default now(),
    updated_at timestamp    not null default now()
) engine = innodb default charset = utf8mb4 collate = utf8mb4_unicode_ci;

-- migrate:down
drop table users;

