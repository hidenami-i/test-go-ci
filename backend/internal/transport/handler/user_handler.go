package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"github.com/template-web-app/internal/domain/entity"
	"github.com/template-web-app/internal/usecase"
	"github.com/template-web-app/pkg/utils"
)

type UserHandler struct {
	userUsecase usecase.UserUsecase
}

func NewUserHandler(userUsecase usecase.UserUsecase) *UserHandler {
	return &UserHandler{
		userUsecase: userUsecase,
	}
}

func (h *UserHandler) GetUser(ctx *gin.Context) {
	id := ctx.Param("id")
	user, err := h.userUsecase.GetUserByID(ctx, id)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, user)
}

func (h *UserHandler) GetAllUsers(ctx *gin.Context) {
	users, err := h.userUsecase.GetAllUsers(ctx)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, users)
}

func (h *UserHandler) CreateUser(ctx *gin.Context) {
	newUser := &entity.User{}
	if err := ctx.ShouldBindJSON(&newUser); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid user data", "message": err.Error()})
		return
	}

	newUser.ID = utils.GenerateID()
	u, err := h.userUsecase.CreateUser(ctx, newUser)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Error creating user", "message": err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, u)
}

func (h *UserHandler) UpdateUser(ctx *gin.Context) {
	user := &entity.User{}
	if err := ctx.ShouldBindJSON(&user); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid user data"})
		return
	}

	u, err := h.userUsecase.UpdateUser(ctx, user)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Error updating user", "message": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, u)
}

func (h *UserHandler) DeleteUser(ctx *gin.Context) {
	//id := ctx.Param("id")
	//err := h.userUsecase.DeleteUser(ctx, id)
	//if err != nil {
	//	ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Error deleting user", "message": err.Error()})
	//	return
	//}

	ctx.Status(http.StatusNoContent)
}
