package handler

import (
	"github.com/gin-gonic/gin"

	"github.com/template-web-app/internal/log"
)

func HandleHTTPError(ctx *gin.Context, err error, status int) {
	log.Error(err.Error())
	//apiError := errors.NewAPIError(err.Error())
	//ctx.JSON(status, apiError.Response())
}
