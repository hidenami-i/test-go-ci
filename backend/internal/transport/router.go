package transport

import (
	"net/http"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"

	"github.com/template-web-app/internal/transport/handler"
	"github.com/template-web-app/internal/usecase"
	"github.com/template-web-app/pkg/middleware"
)

func NewRouter(userUsecase usecase.UserUsecase) *gin.Engine {
	router := gin.Default()

	router.Use(cors.New(cors.Config{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{"GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"},
		AllowHeaders: []string{
			"Origin",
			"Content-Length",
			"Content-Type",
			"Authorization",
			"Access-Control-Allow-Origin",
			"Access-Control-Allow-Headers",
		},
		AllowCredentials: false,
		MaxAge:           24 * time.Hour,
	}))

	// Configure rate limiter
	//limiter := tollbooth.NewLimiter(1, &tollbooth.Config{
	//	Max:                  1,         // Maximum requests per second
	//	ExpirationTTL:        time.Hour, // Time to live for token buckets
	//	DefaultExpirationTTL: time.Hour,
	//})
	//limiter.SetIPLookups([]string{"RemoteAddr", "X-Forwarded-For", "X-Real-IP"})
	//
	//lmt := tollbooth.NewLimiter(1, nil)

	router.Use(middleware.Logger())
	userHandler := handler.NewUserHandler(userUsecase)
	router.GET("/health", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"status": "ok",
		})
	})

	v1 := router.Group("/v1")
	{
		users := v1.Group("/users")
		{
			users.GET("", userHandler.GetAllUsers)
			users.GET("/:id", userHandler.GetUser)
			users.POST("", userHandler.CreateUser)
			users.PUT("/:id", userHandler.UpdateUser)
			users.DELETE("/:id", userHandler.DeleteUser)
		}
	}

	return router
}
