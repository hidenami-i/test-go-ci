package config

import (
	"context"
	"strconv"

	envconfig "github.com/sethvargo/go-envconfig"
)

type AppEnv string

const (
	LocalEnv   AppEnv = "local"
	DevEnv     AppEnv = "dev"
	StagingEnv AppEnv = "staging"
	ProdEnv    AppEnv = "prod"
)

type Config struct {
	ApplicationConfig *ApplicationConfig
	DatabaseConfig    *DatabaseConfig
	LogConfig         *LogConfig
}

type ApplicationConfig struct {
	AppEnv AppEnv `env:"APP_ENV,default=local"`
	Host   string `env:"BACKEND_HOST"`
	Port   string `env:"BACKEND_CONTAINER_PORT"`
}

type DatabaseConfig struct {
	Host                  string `env:"DB_HOST"`
	Port                  int    `env:"DB_PORT"`
	Name                  string `env:"DB_NAME"`
	User                  string `env:"DB_USER"`
	Password              string `env:"DB_PASSWORD"`
	MaxIdleConnections    int    `env:"DB_MAX_IDLE_CONNECTIONS,default=10"`
	MaxOpenConnections    int    `env:"DB_MAX_OPEN_CONNECTIONS,default=10"`
	MaxConnectionLifetime int    `env:"DB_CONNECTION_MAX_LIFETIME,default=60"`
}

type LogConfig struct {
	Level       string `env:"BACKEND_LOG_LEVEL,default=info"`
	Development bool   `env:"BACKEND_LOG_DEVELOPMENT,default=false"`
}

func NewConfig(ctx context.Context) (*Config, error) {
	cfg := &Config{}
	if err := envconfig.Process(ctx, cfg); err != nil {
		return nil, err
	}

	return cfg, nil
}

func (c *DatabaseConfig) GetDSN() string {
	return c.User + ":" + c.Password + "@tcp(" + c.Host + ":" + strconv.Itoa(c.Port) + ")/" + c.Name + "?charset=utf8mb4&parseTime=True&loc=Local"
}

func (ae AppEnv) IsLocal() bool {
	return ae == LocalEnv
}

func (ae AppEnv) IsDev() bool {
	return ae == DevEnv
}

func (ae AppEnv) IsStaging() bool {
	return ae == StagingEnv
}

func (ae AppEnv) IsProd() bool {
	return ae == ProdEnv
}
