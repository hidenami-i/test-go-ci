package sample

import (
	"context"
	"os"
	"time"

	"go.uber.org/zap"
	"gorm.io/gorm"

	"github.com/template-web-app/internal/config"
	"github.com/template-web-app/internal/infrastructure/database"
	"github.com/template-web-app/pkg/utils"
)

func main() {
	ctx := context.Background()
	cfg, err := config.NewConfig(ctx)
	if err != nil {
		os.Exit(1)
	}

	db := database.New(cfg.DatabaseConfig, zap.NewExample())
	err = db.Setup()
	if err != nil {
		os.Exit(1)
	}

	gtm := NewGormTransactionManager(db.DB())
	uRepo := NewUserRepository(db.DB())
	usRepo := NewUserSkillRepository(db.DB())
	usecase := NewUserInteractor(gtm, uRepo, usRepo)

	user := &User{
		ID:        utils.GenerateID(),
		CreatedAt: time.Time{},
		UpdatedAt: time.Time{},
	}
	err = usecase.CreateUser(ctx, user)
	if err != nil {
		os.Exit(1)
	}
}

type TransactionManager interface {
	DoInTx(ctx context.Context, fn func(tx *gorm.DB) error) error
}

type GormTransactionManager struct {
	db *gorm.DB
}

func NewGormTransactionManager(db *gorm.DB) TransactionManager {
	return &GormTransactionManager{
		db: db,
	}
}

func (gtm *GormTransactionManager) DoInTx(ctx context.Context, fn func(*gorm.DB) error) error {
	return gtm.db.WithContext(ctx).Transaction(func(tx *gorm.DB) error {
		return fn(tx)
	})
}

type UserUsecase interface {
	CreateUser(ctx context.Context, user *User) error
}

type userInteractor struct {
	userRepo      IUserRepository
	userSkillRepo IUserSkillRepository
	transaction   TransactionManager
}

func NewUserInteractor(transaction TransactionManager, userRepo IUserRepository, userSkillRepo IUserSkillRepository) UserUsecase {
	return &userInteractor{
		userRepo:      userRepo,
		transaction:   transaction,
		userSkillRepo: userSkillRepo,
	}
}

func (ui *userInteractor) CreateUser(ctx context.Context, user *User) error {
	us := &UserSkill{
		ID:        utils.GenerateID(),
		UserID:    user.ID,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	if err := ui.transaction.DoInTx(ctx, func(tx *gorm.DB) error {
		err := ui.userRepo.Create(ctx, tx, user)
		if err != nil {
			return err
		}

		_, err = ui.userSkillRepo.Create(ctx, tx, us)
		if err != nil {
			return err
		}

		return nil
	}); err != nil {
		return err
	}

	return nil
}

type User struct {
	ID        string    `gorm:"primaryKey" json:"id"`
	CreatedAt time.Time `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt time.Time `gorm:"autoUpdateTime" json:"updated_at"`
}

type IUserRepository interface {
	Create(ctx context.Context, tx *gorm.DB, user *User) error
}

type UserRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) IUserRepository {
	return &UserRepository{
		db: db,
	}
}

func (u *UserRepository) Create(ctx context.Context, tx *gorm.DB, user *User) error {
	err := tx.WithContext(ctx).Create(user).Error
	if err != nil {
		return err
	}
	return nil
}

type UserSkill struct {
	ID        string    `gorm:"primaryKey" json:"id"`
	UserID    string    `json:"user_id"`
	CreatedAt time.Time `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt time.Time `gorm:"autoUpdateTime" json:"updated_at"`
}

type IUserSkillRepository interface {
	Create(ctx context.Context, tx *gorm.DB, us *UserSkill) (*UserSkill, error)
}

type UserSkillRepository struct {
	db *gorm.DB
}

func NewUserSkillRepository(db *gorm.DB) IUserSkillRepository {
	return &UserSkillRepository{
		db: db,
	}
}

func (u *UserSkillRepository) Create(ctx context.Context, tx *gorm.DB, us *UserSkill) (*UserSkill, error) {
	err := tx.WithContext(ctx).Create(us).Error
	if err != nil {
		return nil, err
	}
	return us, nil
}
