package log

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"github.com/template-web-app/internal/config"
)

var (
	logger      *zap.Logger
	atomicLevel zap.AtomicLevel
)

func Initialize(logConfig *config.LogConfig) error {
	level, err := zapcore.ParseLevel(logConfig.Level)
	if err != nil {
		return err
	}

	atomicLevel = zap.NewAtomicLevelAt(level)
	zapCfg := zap.Config{
		Level:             atomicLevel,
		Development:       logConfig.Development,
		DisableCaller:     false,
		DisableStacktrace: false,
		Sampling:          nil,
		Encoding:          "json",
		EncoderConfig: zapcore.EncoderConfig{
			MessageKey:     "message",
			LevelKey:       "level",
			TimeKey:        "timestamp",
			NameKey:        "logger",
			CallerKey:      "caller",
			StacktraceKey:  "stacktrace",
			LineEnding:     zapcore.DefaultLineEnding,
			EncodeLevel:    zapcore.LowercaseLevelEncoder,
			EncodeTime:     zapcore.ISO8601TimeEncoder,
			EncodeDuration: zapcore.SecondsDurationEncoder,
			EncodeCaller:   zapcore.ShortCallerEncoder,
		},
		OutputPaths:      []string{"stdout"},
		ErrorOutputPaths: []string{"stderr"},
		InitialFields:    nil,
	}

	logger, err = zapCfg.Build(zap.AddCallerSkip(1))
	if err != nil {
		return err
	}

	return nil
}

func SetLogLevel(lvl zapcore.Level) {
	atomicLevel.SetLevel(lvl)
}

func Logger() *zap.Logger {
	return logger
}

func Info(args ...interface{}) {
	logger.Sugar().Info(args...)
}

func Infoln(args ...interface{}) {
	logger.Sugar().Infoln(args...)
}

func Infof(format string, args ...interface{}) {
	logger.Sugar().Infof(format, args...)
}

func Debug(args ...interface{}) {
	logger.Sugar().Debug(args...)
}

func Debugln(args ...interface{}) {
	logger.Sugar().Debugln(args...)
}

func Debugf(format string, args ...interface{}) {
	logger.Sugar().Debugf(format, args...)
}

func Warn(args ...interface{}) {
	logger.Sugar().Warn(args...)
}

func Warnln(args ...interface{}) {
	logger.Sugar().Warnln(args...)
}

func Warnf(format string, args ...interface{}) {
	logger.Sugar().Warnf(format, args...)
}

func Error(args ...interface{}) {
	logger.Sugar().Error(args...)
}

func Errorln(args ...interface{}) {
	logger.Sugar().Errorln(args...)
}

func Errorf(format string, args ...interface{}) {
	logger.Sugar().Errorf(format, args...)
}

func Panic(args ...interface{}) {
	logger.Sugar().Panic(args...)
}

func Panicln(args ...interface{}) {
	logger.Sugar().Panicln(args...)
}

func Panicf(format string, args ...interface{}) {
	logger.Sugar().Panicf(format, args...)
}

func Fatal(args ...interface{}) {
	logger.Sugar().Fatal(args...)
}

func Fatalln(args ...interface{}) {
	logger.Sugar().Fatalln(args...)
}

func Fatalf(format string, args ...interface{}) {
	logger.Sugar().Fatalf(format, args...)
}
