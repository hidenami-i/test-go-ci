package interactor

import (
	"context"

	"github.com/template-web-app/internal/domain/entity"
	"github.com/template-web-app/internal/domain/repository"
	"github.com/template-web-app/internal/usecase"
	"github.com/template-web-app/pkg/utils"
)

type userInteractor struct {
	userRepo    repository.UserRepository
	transaction repository.TransactionManager
}

// NewUserInteractor creates a new UserInteractor instance
func NewUserInteractor(transaction repository.TransactionManager, userRepo repository.UserRepository) usecase.UserUsecase {
	return &userInteractor{
		transaction: transaction,
		userRepo:    userRepo,
	}
}

func (ui *userInteractor) CreateUser(ctx context.Context, user *entity.User) (*entity.User, error) {
	newUser := &entity.User{}
	if err := ui.transaction.DoInTx(ctx, func(tx repository.DB) error {
		u, err := ui.userRepo.CreateInTx(ctx, tx, user)
		if err != nil {
			return err
		}

		u.ID = utils.GenerateID()
		_, err = ui.userRepo.CreateInTx(ctx, tx, user)
		if err != nil {
			return err
		}

		newUser = u

		return nil
	}); err != nil {
		return nil, err
	}

	return newUser, nil
}

func (ui *userInteractor) GetAllUsers(ctx context.Context) ([]*entity.User, error) {
	users, err := ui.userRepo.GetAll(ctx)
	if err != nil {
		return nil, err
	}

	return users, nil
}

func (ui *userInteractor) GetUserByID(ctx context.Context, id string) (*entity.User, error) {
	user, err := ui.userRepo.GetByID(ctx, id)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (ui *userInteractor) UpdateUser(ctx context.Context, user *entity.User) (*entity.User, error) {
	u, err := ui.userRepo.Update(ctx, user)
	if err != nil {
		return nil, err
	}

	return u, nil
}
