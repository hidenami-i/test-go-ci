package repository

import (
	"context"

	"github.com/template-web-app/internal/domain/entity"
)

type UserRepository interface {
	GetAll(ctx context.Context) ([]*entity.User, error)
	GetByID(ctx context.Context, id string) (*entity.User, error)
	Create(ctx context.Context, user *entity.User) (*entity.User, error)
	Update(ctx context.Context, user *entity.User) (*entity.User, error)
	CreateInTx(ctx context.Context, tx DB, user *entity.User) (*entity.User, error)
}
