package repository

import "context"

type DB interface {
	Create(value any) error
	Update(column string, value any) error
	Delete(value any) error
	Save(value any) error
	DeleteBy(value any, query any, args ...any) error
	// 必要であれば、他のメソッドを追加する
}

type TransactionManager interface {
	DoInTx(ctx context.Context, fn func(tx DB) error) error
}
