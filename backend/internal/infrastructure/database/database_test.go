package database

import (
	"context"
	"testing"

	envconfig "github.com/sethvargo/go-envconfig"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap"

	"github.com/template-web-app/internal/config"
)

func TestDatabase_Setup(t *testing.T) {
	logger, err := zap.NewDevelopment()
	require.NoError(t, err, "failed to create logger")

	cfg := &config.DatabaseConfig{}
	err = envconfig.Process(context.Background(), cfg)
	require.NoError(t, err, "failed to parse environment variables")

	db := New(cfg, logger)
	err = db.Setup()
	require.NoError(t, err, "failed to initialize database")

	t.Cleanup(func() {
		err = db.Close()
		require.NoError(t, err, "failed to close database")
	})
}
