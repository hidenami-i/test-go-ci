package database

import (
	"time"

	backoff "github.com/cenkalti/backoff/v4"
	"go.uber.org/zap"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"moul.io/zapgorm2"

	"github.com/template-web-app/internal/config"
)

type Database struct {
	db     *gorm.DB
	config *config.DatabaseConfig
	logger *zap.Logger
}

func New(cfg *config.DatabaseConfig, logger *zap.Logger) *Database {
	return &Database{
		config: cfg,
		logger: logger,
	}
}

func (d *Database) Setup() error {
	dsn := d.config.GetDSN()

	bo := backoff.NewExponentialBackOff()
	bo.MaxElapsedTime = 120 * time.Second
	var db *gorm.DB
	if err := backoff.Retry(func() error {
		var err error
		db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{
			Logger: zapgorm2.New(d.logger),
		})
		if err != nil {
			return err
		}
		return nil
	}, bo); err != nil {
		return err
	}

	// set connection pool
	sqlDB, err := db.DB()
	if err != nil {
		return err
	}

	sqlDB.SetMaxIdleConns(d.config.MaxIdleConnections)
	sqlDB.SetMaxOpenConns(d.config.MaxOpenConnections)
	sqlDB.SetConnMaxLifetime(time.Duration(d.config.MaxConnectionLifetime) * time.Second)

	// test database connection
	err = sqlDB.Ping()
	if err != nil {
		return err
	}

	// Store the *gorm.DB instance in the Database struct
	d.db = db

	return nil
}

func (d *Database) Close() error {
	db, err := d.db.DB()
	if err != nil {
		return err
	}
	return db.Close()
}

func (d *Database) DB() *gorm.DB {
	return d.db
}
