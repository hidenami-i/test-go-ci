package repository

import (
	"context"

	"gorm.io/gorm"

	"github.com/template-web-app/internal/domain/entity"
	"github.com/template-web-app/internal/domain/repository"
)

// UserRepository - User repository implementation
type UserRepository struct {
	db *gorm.DB
}

// NewUserRepository creates a new UserRepository instance
func NewUserRepository(db *gorm.DB) repository.UserRepository {
	return &UserRepository{
		db: db,
	}
}

func (ur *UserRepository) CreateInTx(ctx context.Context, tx repository.DB, user *entity.User) (*entity.User, error) {
	err := tx.Create(user)
	return user, err
}

// GetAll gets all users
func (ur *UserRepository) GetAll(ctx context.Context) ([]*entity.User, error) {
	var users []*entity.User
	err := ur.db.Find(&users).Error
	return users, err
}

// GetByID gets a user by ID
func (ur *UserRepository) GetByID(ctx context.Context, id string) (*entity.User, error) {
	user := &entity.User{}
	err := ur.db.First(user, id).Error
	return user, err
}

// Create creates a new user
func (ur *UserRepository) Create(ctx context.Context, user *entity.User) (*entity.User, error) {
	err := ur.db.Create(user).Error
	return user, err
}

// Update updates an existing user
func (ur *UserRepository) Update(ctx context.Context, user *entity.User) (*entity.User, error) {
	err := ur.db.Save(user).Error
	return user, err
}

// DeleteByID deletes a user by ID
func (ur *UserRepository) DeleteByID(ctx context.Context, id string) error {
	return ur.db.Delete(&entity.User{ID: id}).Error
}
