package transaction

import (
	"context"

	"gorm.io/gorm"

	"github.com/template-web-app/internal/domain/repository"
)

type gormTransaction struct {
	tx *gorm.DB
}

var _ repository.DB = &gormTransaction{}

func (gt *gormTransaction) Create(value any) error {
	return gt.tx.Create(value).Error
}

func (gt *gormTransaction) Update(column string, value any) error {
	return gt.tx.Model(value).Update(column, value).Error
}

func (gt *gormTransaction) Delete(value any) error {
	return gt.tx.Delete(value).Error
}

func (gt *gormTransaction) Save(value any) error {
	return gt.tx.Save(value).Error
}

func (gt *gormTransaction) DeleteBy(value any, query any, args ...any) error {
	return gt.tx.Delete(value, query, args).Error
}

type GormTransactionManager struct {
	db *gorm.DB
}

func NewGormTransactionManager(db *gorm.DB) repository.TransactionManager {
	return &GormTransactionManager{
		db: db,
	}
}

func (gtm *GormTransactionManager) DoInTx(ctx context.Context, fn func(tx repository.DB) error) error {
	return gtm.db.WithContext(ctx).Transaction(func(tx *gorm.DB) error {
		return fn(&gormTransaction{tx: tx})
	})
}
