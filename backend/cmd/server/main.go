package main

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/template-web-app/internal/config"
	"github.com/template-web-app/internal/infrastructure/database"
	"github.com/template-web-app/internal/infrastructure/repository"
	"github.com/template-web-app/internal/infrastructure/transaction"
	"github.com/template-web-app/internal/log"
	"github.com/template-web-app/internal/transport"
	"github.com/template-web-app/internal/usecase/interactor"
)

func main() {
	// syscall.SIGINT または syscall.SIGTERM シグナルが受信されると、このコンテキストがキャンセルされます。
	ctx, cancel := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer cancel()

	cfg, err := config.NewConfig(ctx)
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "failed to load config: %v\n", err)
		os.Exit(1)
	}

	err = log.Initialize(cfg.LogConfig)
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "failed to initialize logger: %v\n", err)
		os.Exit(1)
	}
	defer log.Logger().Sync()

	log.Info("Starting server...")

	db := database.New(cfg.DatabaseConfig, log.Logger())
	err = db.Setup()
	if err != nil {
		log.Errorf("failed to initialize database: %w", err)
		os.Exit(1)
		return
	}

	defer func(db *database.Database) {
		err := db.Close()
		if err != nil {
			log.Errorf("failed to close database: %w", err)
			os.Exit(1)
		}
	}(db)

	gormTx := transaction.NewGormTransactionManager(db.DB())
	userRepo := repository.NewUserRepository(db.DB())
	userUsecase := interactor.NewUserInteractor(gormTx, userRepo)
	router := transport.NewRouter(userUsecase)

	srv := &http.Server{
		Addr:    net.JoinHostPort(cfg.ApplicationConfig.Host, cfg.ApplicationConfig.Port),
		Handler: router,
	}

	// 通常メインゴルーチンがHTTPサーバーのリクエストを処理し続けるために利用されます。
	// サーバーを適切にシャットダウンするために、別のシグナル（SIGINTやSIGTERMなど）を待ち受ける必要があります。
	// 新しいゴルーチンを作成して router.Run を実行することで、メインゴルーチンはシグナルを待ち受けることができ、サーバーがシャットダウン要求を受け取ったときに適切に終了処理を行うことができます。
	go func() {
		if err := router.Run(srv.Addr); err != nil && err != http.ErrServerClosed {
			log.Errorf("listen: %w\n", err)
			os.Exit(1)
		}
	}()

	// キャンセルを待ってサーバーのシャットダウンが開始されます
	<-ctx.Done()
	log.Info("Shutting down server...")

	// 新しいcontextを使用してシャットダウン時にサーバーに別の独立したタイムアウトを与え、サーバーは最大5秒間待ってからシャットダウンを強制します
	ctxShutdown, cancelShutdown := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelShutdown()
	if err := srv.Shutdown(ctxShutdown); err != nil {
		log.Errorf("Server forced to shutdown:", err)
		os.Exit(1)
	}

	log.Info("Server exiting")
}
