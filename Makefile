-include ./config/.env
DOCKER_COMPOSE_FILE := ./config/docker-compose.yml
DATABASE_URL := "mysql://$(DB_USER):$(DB_PASSWORD)@db:${DB_HOST_PORT}/$(DB_NAME)"
DBMATE_CMD := docker-compose -f $(DOCKER_COMPOSE_FILE) exec -e DATABASE_URL=$(DATABASE_URL) backend dbmate

##################################################################
# Setup
##################################################################

.PHONY: setup setup-backend setup-frontend setup-env

setup:
	@make setup-env
	@make dc-build
	@make setup-backend
	@make setup-frontend
	@make setup-db
	@make dc-stop
	@echo "complete setup"
	@echo "run 'make dc-up' to start containers"

setup-env:
	@echo "setup .env file..."
	@cp ./config/.env.example ./config/.env

setup-backend:
	@echo "setup backend..."
	@cd ./backend go mod download && cd ../
	@make -C ./backend install-tools

setup-frontend:
	@echo "setup frontend..."
	@cd ./frontend && yarn install && cd ../

setup-db:
	@echo "setup db..."
	@make dc-upd-server
	@echo "waiting for db to be ready..."
	@bash -c 'until docker-compose -f $(DOCKER_COMPOSE_FILE) exec -T db mysqladmin ping --silent; do sleep 5; done;'
#	@echo "waiting for Go server to be ready..."
#	@bash -c 'until curl --output /dev/null --silent --fail http://localhost:$(BACKEND_HOST_PORT)/health; do sleep 5; done;'
	@echo "migration db..."
	@make db-up

wait-db:
	@echo "waiting for db to be ready..."
	@bash -c 'until docker-compose -f $(DOCKER_COMPOSE_FILE) exec -T db mysqladmin ping --silent; do sleep 5; done;'

wait-backend:
	@echo "waiting for Go server to be ready..."
	@bash -c 'until curl --output /dev/null --fail http://backend:$(BACKEND_HOST_PORT)/health; do sleep 5; done;'

##################################################################
# Docker Compose
##################################################################

.PHONY: dc-up dc-up-server dc-upd dc-stop dc-down dc-build dc-rebuild dc-build-no-cache dc-logs dc-ps

# Starts containers in the foreground
dc-up:
	@echo "Starting containers..."
	@docker-compose -f $(DOCKER_COMPOSE_FILE) up --build

# Starts containers in the foreground
dc-up-server:
	@echo "Starting server containers..."
	@docker-compose -f $(DOCKER_COMPOSE_FILE) up --build db backend

# Starts containers in the background (detached mode)
dc-upd:
	@echo "Starting containers in detached mode..."
	@docker-compose -f $(DOCKER_COMPOSE_FILE) up --build --detach

# Starts containers in the background (detached mode)
dc-upd-server:
	@echo "Starting server containers in detached mode..."
	@docker-compose -f $(DOCKER_COMPOSE_FILE) up --build --detach db backend

# Starts containers in the background (detached mode)
dc-upd-db:
	@echo "Starting db containers in detached mode..."
	@docker-compose -f $(DOCKER_COMPOSE_FILE) up --detach db

# Starts containers in the background (detached mode)
dc-upd-backend:
	@echo "Starting backend containers in detached mode..."
	@docker-compose -f $(DOCKER_COMPOSE_FILE) up --detach backend

# Stops containers
dc-stop:
	@echo "Stopping containers..."
	@docker-compose -f $(DOCKER_COMPOSE_FILE) stop

# Stops and removes containers, networks, and volumes
dc-down:
	@echo "Stopping and removing containers..."
	@docker-compose -f $(DOCKER_COMPOSE_FILE) down --volumes --remove-orphans

# Rebuilds containers without using cache
dc-build:
	@echo "Building containers..."
	@docker-compose -f $(DOCKER_COMPOSE_FILE) build

# Rebuilds containers without using cache
dc-build-no-cache:
	@echo "Building containers without cache..."
	@docker-compose -f $(DOCKER_COMPOSE_FILE) build --no-cache

# Removes containers, networks, volumes, and images, then rebuilds containers without using cache
dc-rebuild:
	@echo "Rebuilding containers..."
	@docker-compose -f $(DOCKER_COMPOSE_FILE) down --volumes --remove-orphans --rmi local
	@docker-compose -f $(DOCKER_COMPOSE_FILE) build --no-cache

# Displays logs from containers
dc-logs:
	@docker-compose -f $(DOCKER_COMPOSE_FILE) logs -f

# Displays status of containers
dc-ps:
	@docker-compose -f $(DOCKER_COMPOSE_FILE) ps

# Test
test-go:
	@echo "Running tests..."
	@docker exec ${PROJECT_KEY}-backend go test -v ./...

lint-go:
	@echo "Running linter..."
	@docker exec ${PROJECT_KEY}-backend golangci-lint --version
	@docker exec ${PROJECT_KEY}-backend golangci-lint run ./...

##################################################################
# Database
##################################################################

.PHONE: db-create db-new db-up db-down db-drop db-dump

db-new:
	@$(DBMATE_CMD) new $(name)

db-up:
	@$(DBMATE_CMD) up

db-down:
	@$(DBMATE_CMD) down

db-drop:
	@$(DBMATE_CMD) drop

db-dump:
	@$(DBMATE_CMD) dump

db-reset:
	@$(DBMATE_CMD) drop
	@$(DBMATE_CMD) up
