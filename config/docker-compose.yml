version: '3.9'

x-default: &x-default
  # ログファイルの上限数を3に設定し、ログファイルのサイズを10MBに制限します。
  # これにより、ログファイルが大きくなりすぎることを防ぎ、ディスクの容量を節約できます。
  logging:
    driver: "json-file"
    options:
      max-size: "10m"
      max-file: "3"
  networks:
    - app_network

x-default-environment: &x-default-environment
  TZ: ${TZ}
  APP_ENV: ${APP_ENV}

services:
  db:
    <<: *x-default
    container_name: ${PROJECT_KEY}-db
    image: ${DB_IMAGE}
    platform: ${DB_PLATFORM}
    environment:
      <<: *x-default-environment
      MYSQL_DATABASE: ${DB_NAME}
      MYSQL_ROOT_HOST: "%"
      MYSQL_ROOT_PASSWORD: ${DB_ROOT_PASSWORD}
      MYSQL_USER: ${DB_USER}
      MYSQL_PASSWORD: ${DB_PASSWORD}
      # mysqldumpを使用するため、mysqlのパスワード認証プラグインをmysql_native_passwordに変更する
      MYSQL_AUTHENTICATION_PLUGIN: mysql_native_password
    # データベースのデータを永続化する
    volumes:
      # db_dataという名前のボリュームが、MySQLコンテナ内の/var/lib/mysqlというパスにマウントされます。
      # :nocacheは、キャッシュを無効化するオプションであり、ボリュームからファイルを読み取る際にキャッシュを使用しないことを示します。これにより、データの一貫性を保てます。
      # この設定により、MySQLデータベースのデータがdb_dataボリュームに永続的に保存され、Dockerコンテナの再起動などでもデータを保持できます。
      # これは、データの永続性と可搬性を実現するために一般的に使用されるDockerボリュームの機能の1つです。
      - db_data:/var/lib/mysql
      # mysqlの設定ファイルをマウントする
      - ./mysql/conf.d:/etc/mysql/conf.d
    # コンテナが使用するポートを公開する
    ports:
      - ${DB_HOST_PORT}:${DB_CONTAINER_PORT}
  db-test:
    <<: *x-default
    container_name: ${PROJECT_KEY}-db-test
    image: ${DB_IMAGE}
    platform: ${DB_PLATFORM}
    environment:
      <<: *x-default-environment
      MYSQL_DATABASE: ${TEST_DB_NAME}
      MYSQL_ROOT_HOST: "%"
      MYSQL_ROOT_PASSWORD: ${DB_ROOT_PASSWORD}
      MYSQL_USER: ${DB_USER}
      MYSQL_PASSWORD: ${DB_PASSWORD}
      MYSQL_AUTHENTICATION_PLUGIN: mysql_native_password
    volumes:
      - ./mysql/conf.d:/etc/mysql/conf.d
    ports:
      - ${TEST_DB_HOST_PORT}:${TEST_DB_CONTAINER_PORT}
  backend:
    <<: *x-default
    container_name: ${PROJECT_KEY}-backend
    build:
      # Dockerfileがあるディレクトリを指定する
      context: ../backend
      # contextで指定されたディレクトリ）を基準とした相対パスでDockerfileを指定する
      dockerfile: ./Dockerfile.local
      args:
        BACKEND_IMAGE: ${BACKEND_IMAGE}
        BACKEND_CONTAINER_PORT: ${BACKEND_CONTAINER_PORT}
    environment:
      <<: *x-default-environment
      BACKEND_HOST: ${BACKEND_HOST}
      BACKEND_CONTAINER_PORT: ${BACKEND_CONTAINER_PORT}
      BACKEND_LOG_LEVEL: ${BACKEND_LOG_LEVEL}
      BACKEND_LOG_DEVELOPMENT: ${BACKEND_LOG_DEVELOPMENT}
      CORS_ALLOW_ORIGINS: ${CORS_ALLOW_ORIGINS}
      DB_HOST: db # 127.0.0.1はループバックアドレスなのでHOSTにはdbのサービス名を使用します
      DB_PORT: ${DB_CONTAINER_PORT}
      DB_NAME: ${DB_NAME}
      DB_USER: ${DB_USER}
      DB_PASSWORD: ${DB_PASSWORD}
      DB_MAX_IDLE_CONNECTIONS: ${DB_MAX_IDLE_CONNECTIONS}
      DB_MAX_OPEN_CONNECTIONS: ${DB_MAX_OPEN_CONNECTIONS}
      DB_CONNECTION_MAX_LIFETIME: ${DB_CONNECTION_MAX_LIFETIME}
      TEST_DB_HOST: db-test
      TEST_DB_NAME: ${TEST_DB_NAME}
      TEST_DB_PORT: ${TEST_DB_CONTAINER_PORT}
    ports:
      - ${BACKEND_HOST_PORT}:${BACKEND_CONTAINER_PORT}
    volumes:
      - ../backend:/app
      - ../backend/db:/db
  frontend:
    <<: *x-default
    container_name: ${PROJECT_KEY}-frontend
    build:
      # Dockerfileがあるディレクトリを指定する
      context: ../frontend
      # contextで指定されたディレクトリ）を基準とした相対パスでDockerfileを指定する
      dockerfile: ./Dockerfile.local
      args:
        FRONTEND_IMAGE: ${FRONTEND_IMAGE}
        FRONTEND_CONTAINER_PORT: ${FRONTEND_CONTAINER_PORT}
    ports:
      - ${FRONTEND_HOST_PORT}:${FRONTEND_CONTAINER_PORT}
      - "9229:9229"
    environment:
      <<: *x-default-environment
      # 開発時に役立つ機能（より詳細なエラーメッセージ、デバッグ情報、ソースマップなど）が有効になります。
      # また、開発環境ではパフォーマンス最適化や圧縮が無効化されることもあります。これは、開発時のビルド速度を向上させ、デバッグを容易にするためです。
      NODE_ENV: ${NODE_ENV}
      CHOKIDAR_USEPOLLING: 'true'
    volumes:
      # ホストマシンの frontend ディレクトリをコンテナ内の /app にマウントする
      # ファイル変更の反映を即座にするため、名前付きボリュームではなくバインドマウントを使用します
      - ../frontend:/app

# ボリュームを定義する
volumes:
  db_data:
    # デフォルトでlocalドライバが使用されるが、明示的に指定する
    driver: local

# ネットワークを定義する
networks:
  app_network: