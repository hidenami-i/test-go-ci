# セットアップ方法

```
プロジェクトのルートディレクトリで
$ make setup
```

## 起動と終了
起動する場合は

```
プロジェクトのルートディレクトリで
$ make dc-up
```

終了する場合は

```
プロジェクトのルートディレクトリで
$ make dc-down
```

## 起動確認をする

### backend

以下を実行して、`{"message":"pong"}`と表示されればOK

```
$ curl http://localhost:8080/ping
```

ブラウザで`http://localhost:8080/ping`にアクセスして、`{"message":"Hello World"}`と表示してもOK

### frontend

ブラウザで`http://localhost:3000`にアクセスして、NextJsのページが表示されればOK


# トラブルシューティング

## docker-composeでimageのpullに失敗する

```
dcoker desktopの設定で、General -> Use Docker Compose V2を無効にする
```